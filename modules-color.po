# Vietnamese translation of drupal (6.0)
# Copyright (c) 2008 by the Vietnamese translation team
# Generated from files:
#  color.module,v 1.39 2008/01/23 09:43:25 goba
#  color.install,v 1.2 2006/12/05 05:49:50 dries
#
msgid ""
msgstr ""
"Project-Id-Version: Drupal 6.x\n"
"POT-Creation-Date: 2008-08-16 14:35+0200\n"
"PO-Revision-Date: 2008-08-23 22:11+0700\n"
"Last-Translator: Thanh Hai, Ha <thanhhai.ha@gmail.com>\n"
"Language-Team: Vietnamese Translation Team <thanhhai.ha@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-Language: Vietnamese\n"
"X-Poedit-Country: VIET NAM\n"
"X-Poedit-SourceCharset: utf-8\n"

#: modules/color/color.module:10
msgid "The color module allows a site administrator to quickly and easily change the color scheme of certain themes. Although not all themes support color module, both Garland (the default theme) and Minnelli were designed to take advantage of its features. By using color module with a compatible theme, you can easily change the color of links, backgrounds, text, and other theme elements. Color module requires that your <a href=\"@url\">file download method</a> be set to public."
msgstr "Module color cho phép người quản trị hệ thống có thể thay đổi tông màu nhanh chóng và dễ dàng cho các giao diện cụ thể. Mặc dù không phải giao diện nào cũng hỗ trợ module color, cả hai giao diện Garland (giao diện mặc định) và Minnelli được thiết kế để tận dụng những tính năng này. Sử dụng module color ứng với mỗi giao diện cụ thể, bạn có thể dễ dàng thay đổi màu sắc của các liên kết, các màu nền, văn bản và các phần tử giao diện khác dễ dàng. Module color yêu cầu <a href=\"@url\">phương thức tải tập tin</a> của hệ thống phải được thiết lập là công cộng."

#: modules/color/color.module:11
msgid "It is important to remember that color module saves a modified copy of the theme's specified stylesheets in the files directory. This means that if you make any manual changes to your theme's stylesheet, you must save your color settings again, even if they haven't changed. This causes the color module generated version of the stylesheets in the files directory to be recreated using the new version of the original file."
msgstr "Ghi nhớ rằng module color lưu một bản đã được chỉnh sửa của stylesheets của giao diện được chỉ định ở thư mục chứa các tập tin. Điều này có nghĩa là nếu bạn có thực hiện thay đổi thủ công đối với stylesheet của giao diện, bạn phải lưu thiết lập màu sắc lại lần nữa, thậm chí nếu chúng chưa được thay đổi. Điều này khiến phiên bản stylesheet tạo bởi module color nằm trong thư mục chứa các tập tin được tạo lại sử dụng phiên bản mới của tập tin gốc."

#: modules/color/color.module:12
msgid "To change the color settings for a compatible theme, select the \"configure\" link for the theme on the <a href=\"@themes\">themes administration page</a>."
msgstr "Để thay đổi các thiết lập màu sắc cho một giao diện cụ thể nào đó, chọn liên kết \"cấu hình\" cho giao diện đó ở <a href=\"@themes\">trang quản lý giao diện</a>."

#: modules/color/color.module:13
msgid "For more information, see the online handbook entry for <a href=\"@color\">Color module</a>."
msgstr "Để có thêm thông tin, vui lòng xem sổ tay trực tuyến của <a href=\"@color\">module Color</a>."

#: modules/color/color.module:38
msgid "The color picker only works if the <a href=\"@url\">download method</a> is set to public."
msgstr "Bộ chọn màu chỉ hoạt động nếu <a href=\"@url\">phương thức tải tập tin</a> được thiết lập là công cộng."

#: modules/color/color.module:43
msgid "Color scheme"
msgstr "Phối màu"

#: modules/color/color.module:171
msgid "Custom"
msgstr "Tùy chỉnh"

#: modules/color/color.module:174
msgid "Color set"
msgstr "Bộ màu"

#: modules/color/color.module:182
msgid "Base color"
msgstr "Màu chủ đạo"

#: modules/color/color.module:183
msgid "Link color"
msgstr "Màu liên kết"

#: modules/color/color.module:184
msgid "Header top"
msgstr "Đầu đề trên"

#: modules/color/color.module:185
msgid "Header bottom"
msgstr "Đầu đề dưới"

#: modules/color/color.module:186
msgid "Text color"
msgstr "Màu chữ"

#: modules/color/color.module:271
msgid "There is not enough memory available to PHP to change this theme's color scheme. You need at least %size more. Check the <a href=\"@url\">PHP documentation</a> for more information."
msgstr "Không đủ bộ nhớ trống cho PHP để thay đổi phối màu của giao diện này. Hệ thống cần phải có thêm ít nhất %size. Vui lòng xem <a href=\"%url\">tài liệu PHP</a> để có thêm thông tin."

#: modules/color/color.module:0
msgid "color"
msgstr "color"

#: modules/color/color.install:21
msgid "The GD library for PHP is enabled, but was compiled without PNG support. Please check the <a href=\"@url\">PHP image documentation</a> for information on how to correct this."
msgstr "Thư viện GD cho PHP đã được bật, nhưng đã được biên dịch không hỗ trợ PNG. Vui lòng <a href=\"@url\">tài liệu về hình ảnh của PHP</a> để biết thêm thông tin về cách sửa chữa lỗi này."

#: modules/color/color.install:26
msgid "Not installed"
msgstr "Chưa được cài đặt"

#: modules/color/color.install:28
msgid "The GD library for PHP is missing or outdated. Please check the <a href=\"@url\">PHP image documentation</a> for information on how to correct this."
msgstr "Thư viện GD cho PHP bị thiếu hoặc đã quá cũ. Vui lòng xam <a href=\"@url\">tài liệu về hình ảnh của PHP</a> để biết thêm thông tin về cách sửa chữa lỗi này."

#: modules/color/color.install:31
msgid "GD library"
msgstr "Thư viện GD"

